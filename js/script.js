let userName = prompt("What is your name?");
let userAge = +prompt("What is your age?");

const msgWelcome = `Welcome, ${userName}`;
const msgNotAllowed = "You are not allowed to visit this website";

let msg;

if(userAge < 18) {
    msg = msgNotAllowed;
} else if(userAge <= 22) {
    let isOk = confirm("Are you sure you want to continue?");

    if(isOk) {
        msg = msgWelcome;
    } else {
        msg = msgNotAllowed;
    }
    
} else {
    msg = msgWelcome;
}

alert(msg);